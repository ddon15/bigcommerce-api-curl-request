This project was develop from this repository https://github.com/adambilsing/PHP-cURL-lib-for-Bigcommerce-API for improvement so that it can be used particulary for now in Symfony.

How to use inside the class:

use BigcommerceApiCurlRequest\ApiConnection;

//instantiate new store connection by providing Username and API path and token that you can get from your store admin page

$store = new ApiConnection('Username', 'API path', 'API token

//Call specific methods by specifying Resource such as Products, Customers, etc2x..

$store->get('RESOURCE');

$store->delete('RESOURCE');

$store->post('RESOURCE', $fields);

$store->put('RESOURCE', $fields);
